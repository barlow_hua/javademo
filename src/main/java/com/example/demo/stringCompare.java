package com.example.demo;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @Author ysinjoro
 * @Package com.example.demo
 * @Date 2022/9/12 10:50
 * @Description 比较器
 */
public class stringCompare {
    public static void main(String[] args){
        String[] friends = {"tim","angela","he","ddd"};
        Arrays.sort(friends,new LengthCoparetor());
        System.out.println(Arrays.toString(friends));
    }

}

class LengthCoparetor implements Comparator<String> {
    @Override
    public int compare(String o1, String o2) {
        return o1.length()-o2.length();
    }
}
