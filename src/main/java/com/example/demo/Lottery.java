package com.example.demo;

import java.util.Arrays;

public class Lottery {
    public static void main(String[] args){
        int counts = 100 ;
        int[] numbers = new int[counts];
        for(int i = 0;i < numbers.length;i++){
            numbers[i] = i+1;
        }
        int lotterynum = 2;
        int[] resultnum = new int[lotterynum];
        for (int j = 0;j < lotterynum;j++){
            int r = (int)(Math.random()*counts);
            resultnum[j] = numbers[r];
            numbers[r] = numbers[counts-1];
        }
        Arrays.sort(resultnum);
//        普通方法打印数组
//        for (int tmp:
//             resultnum) {
//            System.out.print(tmp + " ");
//        }
        System.out.print(Arrays.toString(resultnum));
    }
}
